#!/bin/zsh

alias ls='eza -lah --group --smart-group --icons --color-scale all'
alias cat='bat'
alias cd..='cd ..'
alias ..='cd ..'
alias ...='cd ../..'
alias mkdir='mkdir -p'
alias grep='grep --color=auto'
alias paru='systemd-inhibit paru'
alias makepkg='systemd-inhibit makepkg'
alias pacman='systemd-inhibit pacman'

alias rm='rm -i'

alias mv='mv -i'

alias cp='cp -i'

alias top='btm --battery -c --enable_cache_memory  --enable_gpu  -a --network_use_bytes  --network_use_log'
