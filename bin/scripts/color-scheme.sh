#!/bin/zsh

function setDarkScheme() {
	gsettings set org.gnome.desktop.interface color-scheme "prefer-dark"
	gsettings set "org.gnome.desktop.interface" cursor-theme "Bibata-Modern-Ice"
	swaymsg "seat seat0 xcursor_theme Bibata-Modern-Ice"
	sed -i 's/^\(custom_palette\s*=\s*\).*/\1true/' .config/qt6ct/qt6ct.conf
	swaymsg output "*" bg ~/.config/sway/wallpaper_dark.jpg fill

	swaymsg bar top_bar colors statusline '#ffffff'
	swaymsg bar top_bar colors background '#00000088'
	swaymsg bar top_bar colors focused_workspace '#00000088' '#00000088' '#6272a4'
	swaymsg bar top_bar colors inactive_workspace '#00000088' '#00000088' '#f8f8f2'
	swaymsg bar top_bar colors urgent_workspace '#00000088' '#00000088' '#FF5555'
	swaymsg bar top_bar colors binding_mode '#00000088' '#00000088' '#FFB86C'

	echo dark > ~/.cache/color-scheme
}

function setLightScheme() {
	gsettings set org.gnome.desktop.interface color-scheme "prefer-light"
	gsettings set "org.gnome.desktop.interface" cursor-theme "Bibata-Modern-Classic"
	swaymsg "seat seat0 xcursor_theme Bibata-Modern-Classic"
	sed -i 's/^\(custom_palette\s*=\s*\).*/\1false/' .config/qt6ct/qt6ct.conf
	swaymsg output "*" bg ~/.config/sway/wallpaper_light.jpg fill

	swaymsg bar top_bar colors statusline '#000000'
	swaymsg bar top_bar colors background ''#00000088''
	swaymsg bar top_bar colors focused_workspace '#00000088' '#00000088' '#6272a4'
	swaymsg bar top_bar colors inactive_workspace '#00000088' '#00000088' '#f8f8f2'
	swaymsg bar top_bar colors urgent_workspace '#00000088' '#00000088' '#FF5555'
	swaymsg bar top_bar colors binding_mode '#00000088' '#00000088' '#FFB86C'

	echo light > ~/.cache/color-scheme
}

function toggleScheme() {
	if [[ "$(gsettings get org.gnome.desktop.interface color-scheme)" == "'prefer-dark'" ]]; then
		setLightScheme
	else
		setDarkScheme
	fi
}

function restoreScheme() {
	colorScheme=$(cat ~/.cache/color-scheme 2>/dev/null)

	# Check the content and execute the corresponding function
	case $colorScheme in
	*light*) setLightScheme ;;
	*dark*) setDarkScheme ;;
	*) setDarkScheme ;;
	esac
}
