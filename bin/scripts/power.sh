#!/bin/zsh

function power_saver() {
	ryzenadj --stapm-limit=1 --fast-limit=1 --slow-limit=1 --tctl-temp=1 --vrm-current=1 --vrmsoc-current=1 --vrmmax-current=1 --vrmsocmax-current=1 --apu-skin-temp=1 --dgpu-skin-temp=1 --apu-slow-limit=1 --skin-temp-limit=1 --power-saving
	cpupower frequency-set --governor powersave --max 400MHz
	powerprofilesctl set power-saver
	echo "power" | tee /sys/devices/system/cpu/cpu*/cpufreq/energy_performance_preference
	echo "low" >/sys/class/drm/card0/device/power_dpm_force_performance_level
	echo passive | sudo tee /sys/devices/system/cpu/amd_pstate/status
}

function power_eco() {
	ryzenadj --stapm-limit=3000 --fast-limit=3000 --slow-limit=1 --stapm-time=10000 --slow-time=10000 --apu-slow-limit=1 --vrmsoc-current=3000 --vrmsocmax-current=3000 --tctl-temp=70 --apu-skin-temp=50 --power-saving
	cpupower frequency-set -g powersave
	powerprofilesctl set power-saver
	echo "power" | tee /sys/devices/system/cpu/cpu*/cpufreq/energy_performance_preference
	echo "low" >/sys/class/drm/card0/device/power_dpm_force_performance_level
	echo passive | sudo tee /sys/devices/system/cpu/amd_pstate/status 
}

function power_default() {
	ryzenadj --stapm-limit=15000 --fast-limit=15000 --slow-limit=1000 --stapm-time=1000 --slow-time=5000 --apu-slow-limit=1000 --vrmsoc-current=13000 --vrmsocmax-current=15000 --tctl-temp=70 --apu-skin-temp=70 --power-saving
	cpupower frequency-set -g schedutil
	powerprofilesctl set balanced
	echo "balance_power" | tee /sys/devices/system/cpu/cpu*/cpufreq/energy_performance_preference 
	echo "auto" >/sys/class/drm/card0/device/power_dpm_force_performance_level
  echo active | sudo tee /sys/devices/system/cpu/amd_pstate/status 
}

function power_perf() {
	cpupower frequency-set -g performance
	powerprofilesctl set performance
	ryzenadj --stapm-limit=35000 --fast-limit=35000 --slow-limit=35000 --stapm-time=1000 --slow-time=1000 --apu-slow-limit=35000 --vrmsoc-current=35000 --vrmsocmax-current=35000 --tctl-temp=90 --apu-skin-temp=90 --max-performance
	echo "performance" | tee /sys/devices/system/cpu/cpu*/cpufreq/energy_performance_preference
	echo "high" >/sys/class/drm/card1/device/power_dpm_force_performance_level
	#https://www.phoronix.com/news/Linux-cpupower-AMD-P-State
	echo active | sudo tee /sys/devices/system/cpu/amd_pstate/status
}

function power_over() {
	cpupower frequency-set -g performance
	powerprofilesctl set performance
	ryzenadj --stapm-limit=99999 --fast-limit=99999 --slow-limit=99999 --tctl-temp=99999 --vrm-current=99999 --vrmsoc-current=99999 --vrmmax-current=99999 --vrmsocmax-current=99999 --apu-skin-temp=99999 --dgpu-skin-temp=99999 --apu-slow-limit=99999 --skin-temp-limit=99999 --max-performance --enable-oc
	echo "performance" | tee /sys/devices/system/cpu/cpu*/cpufreq/energy_performance_preference
	echo "high" >/sys/class/drm/card1/device/power_dpm_force_performance_level
	#https://www.phoronix.com/news/Linux-cpupower-AMD-P-State
	echo active | sudo tee /sys/devices/system/cpu/amd_pstate/status
}

#$ sudo x86_energy_perf_policy --hwp-epp performance
# sudo x86_energy_perf_policy --hwp-epp balance-performance
# sudo x86_energy_perf_policy --hwp-epp balance_power
# sudo x86_energy_perf_policy --hwp-epp power
# https://www.phoronix.com/news/AMD-Core-Perf-Boost-Linux


function fan_maximum() {
	# ectool --interface=lpc fanduty 100
	echo "level full-speed" | tee /proc/acpi/ibm/fan
}

function fan_auto() {
	# ectool --interface=lpc autofanctrl
	echo "level auto" | tee /proc/acpi/ibm/fan
}

function set_max_battery_charge() {
	# framework_tool --driver portio --charge-limit "$1"
}

function get_fan_rpm() {
	# ectool --interface=lpc pwmgetfanrpm
}
