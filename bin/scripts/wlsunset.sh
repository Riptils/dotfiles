#!/bin/zsh

function enableWlsunset() {
	disableWlsunset

	# Check if ~/.cache/timezoneapi.json does not exist or is older than /etc/localtime(meaning that the timezone has changed)
	if [ ! -e ~/.cache/timezoneapi.json ] || [ ~/.cache/timezoneapi.json -ot /etc/localtime ]; then
		timezone=$(timedatectl show | grep -oP 'Timezone=\K[^ ]+' | awk -F'=' '{print $1}')
		json=$(curl 'https://timezoneapi.io/actions/get-temp-token' -H 'X-Requested-With: XMLHttpRequest')
		token=$(echo "$json" | jq -r '.tmp.token')
		curl -o ~/.cache/timezoneapi.json -s "https://timezoneapi.io/api/timezone/?$timezone&token=$token"
	fi

	# Fallback to Strasbourg lat long
	if ! jq -e '.data.timezone.location' ~/.cache/timezoneapi.json > /dev/null 2>&1; then
	    rm -rf ~/.cache/timezoneapi.json
		echo '{"data":{"timezone":{"location":"48.58344,7.74601"}}}' > ~/.cache/timezoneapi.json
	fi

	latitude=$(cat ~/.cache/timezoneapi.json | jq -r '.data.timezone.location | split(",")[0]')
	longitude=$(cat ~/.cache/timezoneapi.json | jq -r '.data.timezone.location | split(",")[1]')

	wlsunset -l "$latitude" -L "$longitude" -t 2900 -T 4000 &

	touch ~/.cache/wlsunset-state
}

function cycleWlsunsetMode() {
	kill -s USR1 $(pgrep wlsunset)
}

function disableWlsunset() {
	killall wlsunset

	rm -rf ~/.cache/wlsunset-state
}

function toggleWlsunset() {
	if pgrep -x "wlsunset" >/dev/null; then
		disableWlsunset
	else
		enableWlsunset
	fi
}

function restoreWlsunset() {
	if [[ -e ~/.cache/wlsunset-state ]]; then
		enableWlsunset
	else
		disableWlsunset
	fi
}