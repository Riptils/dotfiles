#!/bin/zsh

for script in /home/cadwal/bin/scripts/*.sh; do
    if [ -f "$script" ] && [ -x "$script" ]; then
        . "$script"
    fi
done

"$@"
