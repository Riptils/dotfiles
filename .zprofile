[ "$(tty)" = "/dev/tty1" ] && exec sway
[ "$(tty)" = "/dev/tty2" ] && exec sway --debug --verbose 2>&1 | tee "/tmp/sway-debug-verbose-$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 8).txt"

