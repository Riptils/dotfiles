# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

function zant_update() {
  # To avoid losing config, setup antidote only if we are connected to the internet
  if ! ping -q -c 1 github.com &> /dev/null; then
    echo "You are not connected to internet. Can't update."
    return
  fi

  # Clean old setup
  rm -rf .cache/antidote
  rm -rf .config/antidote/*.zwc || true
  rm -rf .*.zwc || true

  zstyle ':antidote:bundle:*' zcompile 'yes'
  zstyle ':antidote:static' zcompile 'yes'
  mkdir -p .cache/antidote

  # Generate Antidote plugins file from the list of zsh plugins
  source /usr/share/zsh-antidote/antidote.zsh
  antidote bundle < "$HOME/.config/antidote/zsh_plugins.txt" > "$HOME/.cache/antidote/zsh_plugins.zsh"

  # Zcompile files
  for file in .zshrc .zshenv .zprofile "$HOME/.cache/antidote/zsh_plugins.zsh"; do
    rm -rf "$file.zwc"
    zcompile "$file"
  done
}

# Check if the Antidote plugins file is not newer than the list of zsh plugins
# or if the Antidote cache is older than 2 weeks (1209600 seconds).
if [[ ! "$HOME/.cache/antidote/zsh_plugins.zsh" -nt "$HOME/.config/antidote/zsh_plugins.txt" || $(($(date +%s) - $(stat -c %Y "$HOME/.cache/antidote"))) -gt 1209600 ]]; then
    echo "-----------------------------------------------"
    echo "         Antidote Update Notification"
    echo "-----------------------------------------------"
    echo "  It's been 2 weeks since the latest update."
    echo "-----------------------------------------------"
    echo "  [y] Update now"
    echo "  [n] Ask me again next time (default)"
    echo "  [s] Skip for another 2 weeks"         
    echo "-----------------------------------------------"

    read -r "response?Proceed with update? [y/N/s]"
    response="${response:-n}"
    response="${response:0:1:l}"

    # Process the user's response
    case $response in
      [y])
        zant_update
        ;;
      [n])
        ;;
      [s])
        touch ~/.cache/antidote
        ;;
      *)
        echo "Invalid response. Please enter y, n, or s"
        ;;
    esac
fi
source "$HOME/.cache/antidote/zsh_plugins.zsh"

# pnpm
export PNPM_HOME="/home/gregoire/.local/share/pnpm"
case ":$PATH:" in
  *":$PNPM_HOME:"*) ;;
  *) export PATH="$PNPM_HOME:$PATH" ;;
esac
# pnpm end

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh